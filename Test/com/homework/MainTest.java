package com.homework;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by Valeriy on 28.09.2016.
 */
public class MainTest {

    @Test
    public void TestMinOfThree(){
        assertEquals(3, Main.minOfThree(3, 5, 8));
    }

    @Test
    public void TestFactorial(){
        assertEquals(120,Main.factorial(5));
    }

    @Test
    public void TestCompareString(){
        String s = new String("This is homework");
        String s2 = new String("This is homework");
        assertTrue(Main.compareString(s,s2));
    }

    @Test
    public void TestConcatenateString(){
        assertEquals("--hello!!", Main.concatenateString("--","hello","!!"));
    }

    @Test
    public void TestFindMinInArray(){
        int arr[] = {47,5,6,9,83,4,20};
        assertEquals(4,Main.findMinInArray(arr));
    }

    @Test
    public void TestSortArray() {
        int arr[] = {47, 5, 6, 9, 83, 4, 20};
        int result[] = {4, 5, 6, 9, 20, 47, 83};
        assertArrayEquals(result, Main.sortArray(arr));
    }

    @Test
    public void TestSumOfElementsUnderMainDiagonal(){
        int[][] matrix = {
                        {10,20,30,40},
                        {10,20,30,40},
                        {10,20,30,40},
                        {10,20,30,40}};
        int result=100;
        assertEquals(result,Main.sumOfElementsUnderMainDiagonal(matrix));
    }

    @Test
    public void TestCountPowerOfTwo(){
        int[][] matrix = {
                {10,2,30,40},
                {10,20,30,40},
                {10,20,30,4},
                {10,20,30,8}};
        assertEquals(3,Main.countPowerOfTwo(matrix));
    }

    @Test(timeout = 1000)
    public void TestSleepFor1000() throws InterruptedException {
        Main.sleepFor1000();
    }

    @Test
    public void TestThrowException() throws Exception {
        Main.throwException();
    }
}
